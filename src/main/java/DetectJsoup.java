import com.sanityinc.jargs.CmdLineParser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

public class DetectJsoup {
    public static void main (String[] args) throws IOException, InterruptedException {

        CmdLineParser cmdLineParser = new CmdLineParser();
        CmdLineParser.Option<String> filePath = cmdLineParser.addStringOption("filePath");

        try {
            cmdLineParser.parse(args);
        } catch (CmdLineParser.OptionException e) {
            System.out.println("Problem in parsing the command line arguments --filePath ");
            System.exit(1);
            e.printStackTrace();
        }

        String pathOfTheFile = cmdLineParser.getOptionValue(filePath);
        if(pathOfTheFile == null) {
            System.out.println("No File path provided. Please provide a file path with param \"--filePath\"");
            System.exit(1);
        }

        FileInputStream excelFile = new FileInputStream(new File(pathOfTheFile));
        Workbook workbook = new XSSFWorkbook(excelFile);
        Sheet datatypeSheet = workbook.getSheet("Social bookmarking");
        if (datatypeSheet == null) {
            System.out.println("\"Social bookmarking\" sheet not found in the workbook");
            System.exit(1);
        }
        Iterator<Row> iterator = datatypeSheet.iterator();

        String queryString = null;
        int rowCount = 0;
        while (iterator.hasNext()) {
            if(rowCount == 0) {
                rowCount++;
                Row currentRow = iterator.next();
                Cell cell = currentRow.createCell(currentRow.getLastCellNum());
                cell.setCellValue("Contains Side Link");
            }
            else {
                Row currentRow = iterator.next();
                Cell cell = currentRow.getCell(1);
                queryString = cell.getStringCellValue();
                if(!(queryString == null || queryString.length() == 0)) {
                    boolean hasSideLink = hasSideLinkPresent (queryString);
                    Cell lastCell = currentRow.createCell(currentRow.getLastCellNum());
                    lastCell.setCellValue(hasSideLink);
                }
            }
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(new File(pathOfTheFile));
            workbook.write(outputStream);
            workbook.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static boolean hasSideLinkPresent(String queryString) throws InterruptedException {
        String chrome_ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
        String url = "https://www.google.com/search?q=" + queryString;
        Connection.Response response = null;
        Document doc = null;
        try {
            doc = Jsoup.connect(url).userAgent(chrome_ua).timeout(5000).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread.sleep(1000);
        boolean containsSideLinks = false;
        if(doc != null) {
            containsSideLinks = doc.toString().contains("<span class=\"cNifBc\">");
            System.out.println(containsSideLinks);
        }
        return containsSideLinks;
    }
}
